/**
 * @file
 * @author Martin Stejskal
 * @brief User/project specific module for UI service code
 */
// ===============================| Includes |================================
#include "ui_service_user.h"

#include <driver/gpio.h>
#include <driver/uart.h>
#include <esp_err.h>
#include <esp_log.h>
#include <string.h>

#include "cfg.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "ui_service_core.h"
#include "ui_service_utils.h"
// ================================| Defines |================================
/**
 * @brief Default password
 *
 * This password will be used until user does not define own password
 */
#define _DEFAULT_PASSWORD ("nopenope")

#define _DEFAULT_NAME ("Mr. nobody")
#define _DEFAULT_NUMBER (42)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char* tag = "UI service (u)";

static const ts_usc_cmd_cfg mas_user_cmds[] = {
    {
        .pac_command = "get name",
        .pac_help = "Return previously set name",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_get_name,
    },
    {
        .pac_command = "set name",
        .pac_help = "Set new name",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_name,

        .s_arg_cfg = {.e_arg_type = UI_SRVCE_CMD_ARG_TYPE_STR,
                      .pac_argument_example = "John Potato"},
    },
    {
        .pac_command = "say hello",
        .pac_help = "Say hello to defined name. Also here is example of"
                    " wrapping"
                    " relatively long description of argument. So remember"
                    " that you do not need to worry about it.",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_say_hello,
    },
    {
        .pac_command = "get number",
        .pac_help = "Return previously stored number of dig up some default",
        .b_require_argument = false,
        .pf_cmd_callback = usu_cmd_get_number,
    },
    {
        .pac_command = "set number",
        .pac_help = "Set new number into system",
        .b_require_argument = true,
        .pf_cmd_callback = usu_cmd_set_number,
        .s_arg_cfg =
            {
                .e_arg_type = UI_SRVCE_CMD_ARG_TYPE_INT,
                .i32_min = -10000,
                .i32_max = 10000,
                .pac_argument_example = "5678",
            },
    },
    {
        .pac_command = "default",
        .pac_help = "Restore default settings",
        .b_require_argument = false,
        .pf_cmd_callback = usu_restore_factory_settings,
    },
};

/**
 * @brief Static variables which are used by commands
 *
 * @{
 */
static char mpac_name[30] = {_DEFAULT_NAME};
static int16_t mi16_number = _DEFAULT_NUMBER;
/**
 * @}
 */

/**
 * @brief Keep track about UART driver state
 */
static bool mb_uart_driver_installed = false;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
const char* usu_get_password(void) { return _DEFAULT_PASSWORD; }

inline void usu_set_service_mode(bool b_service_mode) {
  // Can call some high level application function which can postpone other
  // activities and let service mode do all what is needed. Right now there is
  // just dummy log
  ESP_LOGI(tag, "Setting mode to %d", b_service_mode);
}
// ==========================| Low level functions |==========================
const ts_usc_cmd_cfg* usu_get_list_of_cmds(void) { return mas_user_cmds; }

uint16_t usu_get_num_of_cmds(void) {
  return (sizeof(mas_user_cmds) / sizeof(mas_user_cmds[0]));
}

void usu_delay_ms(const uint16_t u16_delay_ms) {
  const TickType_t x_tick_delay = u16_delay_ms / portTICK_PERIOD_MS;
  if (x_tick_delay) {
    vTaskDelay(x_tick_delay);
  } else {
    // If delay value is zero, wait at least 1 tick
    vTaskDelay(1);
  }
}
// ===============================| Commands |================================
void usu_cmd_get_name(ts_usc_cb_args* ps_cb_args) { usc_print_str(mpac_name); }

void usu_cmd_set_name(ts_usc_cb_args* ps_cb_args) {
  // Need to copy also NULL -> +1
  size_t i_input_length = strlen(ps_cb_args->pac_args) + 1;

  if (i_input_length > sizeof(mpac_name)) {
    // This would not fit into static variable -> fail
    usc_print_str(usc_txt_fail());
  } else {
    // Input string will fit into buffer -> safe to write
    memcpy(mpac_name, ps_cb_args->pac_args, i_input_length);

    usc_print_str(usc_txt_ok());
  }
}

void usu_cmd_say_hello(ts_usc_cb_args* ps_cb_args) {
  usc_printf("Hello %s", mpac_name);
}

void usu_cmd_get_number(ts_usc_cb_args* ps_cb_args) {
  usc_printf("Magic number: %d", mi16_number);
}

void usu_cmd_set_number(ts_usc_cb_args* ps_cb_args) {
  mi16_number = ps_cb_args->u_value.i32_value;

  usc_print_str(usc_txt_ok());
}

void usu_restore_factory_settings(ts_usc_cb_args* ps_cb_args) {
  ESP_LOGI(tag, "Restoring factory settings...");

  memcpy(mpac_name, _DEFAULT_NAME, sizeof(_DEFAULT_NAME));

  mi16_number = _DEFAULT_NUMBER;
}

// ===========================| Interface related |===========================
te_usc_error_code usu_uart_init(void) {
  esp_err_t e_err_code;
  // =================================| UART |==============================
  const uart_config_t s_uart_config = {.baud_rate = UI_SERVICE_UART_BAUDRATE,
                                       .data_bits = UART_DATA_8_BITS,
                                       .parity = UART_PARITY_DISABLE,
                                       .stop_bits = UART_STOP_BITS_1,
                                       .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
  // Try to remove driver if already installed
  if (mb_uart_driver_installed) {
    uart_driver_delete(UI_SERVICE_UART_INTERFACE);
  }

  e_err_code = uart_param_config(UI_SERVICE_UART_INTERFACE, &s_uart_config);
  if (e_err_code) {
    ESP_LOGE(tag, "UART configuration failed");
    return ESP_FAIL;
  }

  e_err_code =
      uart_set_pin(UI_SERVICE_UART_INTERFACE, UI_SERVICE_TXD_PIN,
                   UI_SERVICE_RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
  if (e_err_code) {
    ESP_LOGE(tag, "UART pin set fail");
    return ESP_FAIL;
  }

  e_err_code = uart_driver_install(UI_SERVICE_UART_INTERFACE,
                                   UI_SERVICE_ITF_BUFFER_SIZE, 0, 0, NULL, 0);
  if (e_err_code) {
    ESP_LOGE(tag, "UART driver install failed: %d", e_err_code);
    return ESP_FAIL;
  } else {
    mb_uart_driver_installed = true;
  }
  return ESP_OK;
}

te_usc_error_code usu_uart_read(uint8_t* pu8_data,
                                const uint16_t u16_buffer_length,
                                const uint16_t u16_timeout_ms,
                                uint16_t* pu_num_of_read_bytes) {
  int i_rx_bytes_cnd =
      uart_read_bytes(UI_SERVICE_UART_INTERFACE, pu8_data, u16_buffer_length,
                      u16_timeout_ms / portTICK_PERIOD_MS);

  // Really does not expect to receive more than 65k of data in 1 batch
  assert(i_rx_bytes_cnd <= 0xFFFF);

  *pu_num_of_read_bytes = i_rx_bytes_cnd;

  return USC_OK;
}

te_usc_error_code usu_uart_write(const uint8_t* pu8_data,
                                 const uint16_t u16_data_length,
                                 const uint16_t u16_timeout_ms) {
  int i_tx_bytes =
      uart_write_bytes(UI_SERVICE_UART_INTERFACE, pu8_data, u16_data_length);

  if (i_tx_bytes != u16_data_length) {
    ESP_LOGE(tag, "Transmitted %d but expected to transmit %d", i_tx_bytes,
             u16_data_length);
    return USC_ERROR;
  }

  TickType_t ticks_to_wait = u16_timeout_ms / portTICK_PERIOD_MS;
  // Should not be zero. At least 1 tick should be given
  if (ticks_to_wait == 0) {
    ticks_to_wait = 1;
  }

  esp_err_t e_err_uart_hal =
      uart_wait_tx_done(UI_SERVICE_UART_INTERFACE, ticks_to_wait);

  if (e_err_uart_hal == ESP_ERR_TIMEOUT) {
    return USC_ERROR_TIMEOUT;

  } else if (e_err_uart_hal) {
    return USC_ERROR;

  } else {
    return USC_OK;
  }
}

te_usc_error_code usu_uart_deinit(void) {
  // Try to remove driver no matter what
  uart_driver_delete(UI_SERVICE_UART_INTERFACE);

  // Disable also GPIO
  gpio_pad_select_gpio(UI_SERVICE_TXD_PIN);
  gpio_pad_select_gpio(UI_SERVICE_RXD_PIN);

  gpio_config_t sCfg = {
      .pin_bit_mask = BIT64(UI_SERVICE_TXD_PIN) | BIT64(UI_SERVICE_RXD_PIN),
      .mode = GPIO_MODE_DISABLE,
      .pull_up_en = false,
      .pull_down_en = false,
      .intr_type = GPIO_INTR_DISABLE,
  };
  gpio_config(&sCfg);

  return USC_OK;
}

// ==========================| Internal functions |===========================
