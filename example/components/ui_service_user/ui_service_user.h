/**
 * @file
 * @author Martin Stejskal
 * @brief User/project specific module for UI service code
 */
#ifndef __UI_SERVICE_USER_H__
#define __UI_SERVICE_USER_H__
// ===============================| Includes |================================
#include "ui_service_core.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
/**
 * @brief Returns password needed for successful login
 * @return Pointer to password string
 */
const char* usu_get_password(void);

/**
 * @brief Ideally this should postpone any high level functions
 *
 * In this example, this function is empty. But simply it should allow to let
 * upper layers know that now device is switching into "service mode" and
 * upper layer might want to disable some features (like user input, WiFi,
 * etc.)
 *
 * @param b_service_mode Set 1 if entering service mode, 0 when leaving service
 *                       mode
 */
void usu_set_service_mode(bool b_service_mode);
// ==========================| Low level functions |==========================
/**
 * @brief Returns list of available commands
 * @return List of commands as pointer to static list
 */
const ts_usc_cmd_cfg* usu_get_list_of_cmds(void);

/**
 * @brief Returns number of available commands
 * @return Number of available commands
 */
uint16_t usu_get_num_of_cmds(void);

/**
 * @brief Delay function for UI service core
 * @param u16_delay_ms Input delay in milliseconds
 */
void usu_delay_ms(const uint16_t u16_delay_ms);
// ===============================| Commands |================================
/**
 * @brief Application specific commands
 *
 * These does not have to be public, but it is not bad idea to have them as
 * public functions. It will be more clear that they are used by UI service
 * core. Also they can be eventually used by application layer
 *
 * @{
 */
void usu_cmd_get_name(ts_usc_cb_args* ps_cb_args);
void usu_cmd_set_name(ts_usc_cb_args* ps_cb_args);

void usu_cmd_say_hello(ts_usc_cb_args* ps_cb_args);

void usu_cmd_get_number(ts_usc_cb_args* ps_cb_args);
void usu_cmd_set_number(ts_usc_cb_args* ps_cb_args);

/**
 * @brief Function for restoring factory defaults
 *
 * Simply helps to reset all custom settings
 */
void usu_restore_factory_settings(ts_usc_cb_args* ps_cb_args);
/**
 * @}
 */

// ===========================| Interface related |===========================
/**
 * @brief Initialize service user interface
 *
 * @return USC_OK if no error
 */
te_usc_error_code usu_uart_init(void);

/**
 * @brief API for reading from service UI interface
 *
 * @param[out] pu8_data Read data will be written here
 * @param u16_buffer_length Define data buffer length in Bytes
 * @param u16_timeout_ms Maximum acceptable timeout when reading from interface
 * @param[out] pu_num_of_read_bytes Returns number of read Bytes
 * @return USC_OK if no error
 */
te_usc_error_code usu_uart_read(uint8_t* pu8_data,
                                const uint16_t u16_buffer_length,
                                const uint16_t u16_timeout_ms,
                                uint16_t* pu_num_of_read_bytes);

/**
 * @brief API for writing to service UI interface
 *
 * @param[in] pu8_data Data will be read from there
 * @param u16_data_length Define write data length
 * @param u16_timeout_ms Maximum acceptable timeout when writing to interface
 * @return USC_OK if no error
 */
te_usc_error_code usu_uart_write(const uint8_t* pu8_data,
                                 const uint16_t u16_data_length,
                                 const uint16_t u16_timeout_ms);

/**
 * @brief Deinitialize service user interface
 *
 * This allow to reuse interface for other functionality if needed
 *
 * @return USC_OK if no error
 */
te_usc_error_code usu_uart_deinit(void);
#endif  // __UI_SERVICE_USER_H__
