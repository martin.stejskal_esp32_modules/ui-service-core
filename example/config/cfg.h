/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for example project
 *
 * Mainly here is HW configuration
 */
#ifndef __CFG_H__
#define __CFG_H__
// ===============================| Includes |================================
// ==================================| I/O |==================================
// ==============================| Service UI |===============================
/**
 * @brief Service UI IO
 *
 * @{
 */
/**
 * @brief Service UI baudrate
 */
#define UI_SERVICE_UART_BAUDRATE (115200)

/**
 * @brief Service UI UART interface
 */
#define UI_SERVICE_UART_INTERFACE (UART_NUM_1)

/**
 * @brief Service UI TX pin
 */
#define UI_SERVICE_TXD_PIN (GPIO_NUM_17)

/**
 * @brief Service UI RX pin
 */
#define UI_SERVICE_RXD_PIN (GPIO_NUM_16)
/**
 * @}
 */

/**
 * @brief Interface buffer size in Bytes
 *
 * Usually commands should be really short, so something like 256 should be
 * more than enough.
 *
 * @note For some reason, size under 128 is not allowed. Just be warned
 */
#define UI_SERVICE_ITF_BUFFER_SIZE (512)
// ===========================| Structures, enums |===========================
#endif  // __CFG_H__
