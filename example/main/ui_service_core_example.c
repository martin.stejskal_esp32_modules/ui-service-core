/**
 * @file
 * @author Martin Stejskal
 * @brief Main file for demonstrating UI service core
 */
// ===============================| Includes |================================
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "ui_service_core.h"
#include "ui_service_user.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================
static char *tag = "main";
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void ui_service_core_rtos_task(void *pv_args);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
void app_main(void) {
  ESP_LOGI(tag, "Creating tasks...");

  // Service UI. Allow to setup some fine settings
  xTaskCreate(ui_service_core_rtos_task, "Serv UI", 4 * 1024, NULL,
              tskIDLE_PRIORITY, NULL);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static void ui_service_core_rtos_task(void *pv_args) {
  // Configure task
  ts_usc_config_args s_args = {
      .pf_read_itf_cb = usu_uart_read,
      .pf_write_itf_cb = usu_uart_write,
      .pf_get_password_cb = usu_get_password,
      .pf_get_list_of_cmds_cb = usu_get_list_of_cmds,
      .pf_get_num_of_cmds_cb = usu_get_num_of_cmds,

      // No need to notify application layer when entering/leaving service mode
      .pf_enter_service_mode_cb = 0,
      .pf_enter_service_mode_is_ready_cb = 0,
      .pf_leave_service_mode_cb = 0,
      .pf_leave_service_mode_is_ready_cb = 0,

      .pf_delay_ms_cb = usu_delay_ms,
  };

  te_usc_error_code e_err_code = usc_configure(&s_args);

  if (e_err_code) {
    ESP_LOGE(tag, "Configuration of UI service failed: %s",
             usc_error_code_to_string(e_err_code));
    vTaskDelete(0);
    return;
  }

  // Initialize service UART
  e_err_code = usu_uart_init();
  if (e_err_code) {
    ESP_LOGE(tag, "Service UART initialization failed");
    vTaskDelete(0);
    return;
  }

  // There can be some additional logic, which allow to exit from loop. Maybe
  // in the future...
  while (1) {
    usc_task_exec();

    vTaskDelay(UI_SERVICE_TASK_RECOMMENDED_PERIOD_MS / portTICK_PERIOD_MS);
  }

  // Deinitialize service UART
  e_err_code = usu_uart_deinit();
  if (e_err_code) {
    ESP_LOGE(tag, "Service UART deinitialization failed");
    vTaskDelete(0);
    return;
  }

  // Quit task gracefully
  vTaskDelete(0);
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
