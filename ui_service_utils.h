/**
 * @file
 * @author Martin Stejskal
 * @brief Provide basic conversion functionality between string and integer
 */
#ifndef __STR_INT_CONV_H__
#define __STR_INT_CONV_H__
// ===============================| Includes |================================
// Standard
#include <stdint.h>

// Need error codes
#include "ui_service_core.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Convert array of string values into 32 bit signed values
 *
 * @param[in] pac_str Pointer to string values split by comma
 * @param u8_num_of_items Expected number of values in the string stream
 * @param[out] pi32_buffer Output buffer. Converted values will be stored here
 * @return USC_OK if no error, USC_ERROR_INVALID_PARAMETER if number of items
 *         does not match or if string can not be converted
 */
te_usc_error_code usu_str_dec_array_to_int_32(const char *pac_str,
                                              const uint8_t u8_num_of_items,
                                              int32_t *pi32_buffer);

/**
 * @brief Convert string (decimal number) to 32 bit signed integer
 *
 * @param[in] pac_str Pointer to string
 * @param[out] pi32_result Result will be written here
 * @return USC_OK if no error
 */
te_usc_error_code usu_str_dec_to_int_32(const char *pac_str,
                                        int32_t *pi32_result);

/**
 * @brief Convert string (decimal number) to 32 bit unsigned integer
 *
 * @param[in] pac_str Pointer to string
 * @param[out] pu32_result Result will be written here
 * @return USC_OK if no error
 */
te_usc_error_code usu_str_dec_to_uint_32(const char *pac_str,
                                         uint32_t *pu32_result);

/**
 * @brief Convert string (hexadecimal number) to 32 bit unsigned integer
 *
 * @param[in] pac_str Pointer to string
 * @param[out] pu32_result Result will be written here
 * @return USC_OK if no error
 */
te_usc_error_code usu_str_hex_to_uint_32(const char *pac_str,
                                         uint32_t *pu32_result);

/**
 * @brief Convert string (hexadecimal number) to array of Bytes (Big endian)
 *
 * @param[in] pac_str Pointer to string
 * @param u16_buffer_size Size of buffer. Need to know just to avoid overwriting
 *                      "wrong" memory
 * @param[out] pau8_bytes Pointer to buffer where data will be written
 * @param[out] pu16_num_of_bytes Number of written Bytes into buffer
 * @return USC_OK if no error
 */
te_usc_error_code usu_str_hex_to_bytes(const char *pac_str,
                                       const uint16_t u16_buffer_size,
                                       uint8_t *pau8_bytes,
                                       uint16_t *pu16_num_of_bytes);

/**
 * @brief Convert array of Bytes (Big endian) to string (hexadecimal numbers)
 *
 * @param[in] pau8_bytes Pointer to binary data array
 * @param u16_num_of_bytes Number of Bytes to be processed from binary data
 * array
 * @param c_separator Separator character. This will be inserted between
 *                   HEX characters. If value is zero (NULL) it means nothing
 *                   will be put between converted characters. For instance,
 *                   if you want to get CSV format, set this character to
 *                   space
 * @param[out] pac_str Define where string will be written
 * @param u16_buffer_size Size of string buffer. Knowing this value helps to
 *                      avoid overwriting "bad" memory
 * @return USC_OK if no error
 */
te_usc_error_code usu_bytes_to_str_hex(const uint8_t *pau8_bytes,
                                       const uint16_t u16_num_of_bytes,
                                       const char c_separator, char *pac_str,
                                       const uint16_t u16_buffer_size);
// ========================| Middle level functions |=========================
/**
 * @brief Return length of current word
 *
 * Note that pointer have to be set to first character of word. Algorithm
 * searches for space or NULL character.
 *
 * @param pac_str Pointer to input string
 * @return Length of current word
 */
uint16_t usu_get_word_length(const char *pac_str);
// ==========================| Low level functions |==========================
/**
 * @brief Convert 2 bytes from string into integer
 *
 * @param[in] pac_str Pointer to string with 2 Bytes of HEX value as string
 * @param[out] pu8_result Converted value will be written there
 * @return USC_OK if no error
 */
te_usc_error_code usu_str_hex_2_byte_to_int(const char *pac_str,
                                            uint8_t *pu8_result);

/**
 * @brief Convert character ASCII HEX value to integer
 *
 * @param c_hex HEX value in ASCII
 * @param[out] pu8_result Result will be written here
 * @return USC_OK if no error
 */
te_usc_error_code usu_char_hex_to_int(const char c_hex, uint8_t *pu8_result);

/**
 * @brief Convert Byte to ASCII hex characters
 * @param u8_byte Byte as raw value
 * @param[out] pc_hex Pointer to string. The 2 characters will be used. Make
 * sure there is space for that.
 * @return USC_OK if no error
 */
te_usc_error_code usu_int_to_char_hex(const uint8_t u8_byte, char *pc_hex);
#endif  // __STR_INT_CONV_H__
