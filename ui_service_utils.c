/**
 * @file
 * @author Martin Stejskal
 * @brief Provide basic conversion functionality between string and integer
 */
// ===============================| Includes |================================
#include "ui_service_utils.h"

#include <errno.h>
#include <stdlib.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Convert string in given numeric system to long
 * @param pac_str Pointer to string
 * @param pl_result Result value will be written here
 * @param i_base Numeric base (10 for decimal, 16 for hexadecimal and so on)
 * @return USC_OK if no error
 */
static te_usc_error_code _str_to_long(const char *pac_str, long *pl_result,
                                      const int i_base);

/**
 * @brief Convert input 4 bit value (low) into ASCII hex character
 *
 * @param u8_val_4_bit Expected 4 bit value. If higher, error will be returned
 * @param pc_ascii_hex ASCII hex will be written here
 * @return USC_OK if no error
 */
static te_usc_error_code _4_bit_val_to_hex_char(const uint8_t u8_val_4_bit,
                                                char *pc_ascii_hex);
// =========================| High level functions |==========================
te_usc_error_code usu_str_dec_array_to_int_32(const char *pac_str,
                                              const uint8_t u8_num_of_items,
                                              int32_t *pi32_buffer) {
  // Check input arguments
  if ((!pac_str) || (!pi32_buffer)) {
    return USC_ERROR_EMPTY_POINTER;
  }

  // Expect at least 1 item in parsed field. Otherwise using this function does
  // not make any sense
  if (!u8_num_of_items) {
    return USC_ERROR_INVALID_PARAMETER;
  }

  te_usc_error_code e_err_code = USC_ERROR;

  for (uint8_t u8_item_idx = 0; u8_item_idx < u8_num_of_items; u8_item_idx++) {
    // Convert string to number. If fails, exit loop now
    e_err_code = usu_str_dec_to_int_32(pac_str, pi32_buffer);

    if (e_err_code) {
      break;
    }

    // Search for separator character, if not last iteration
    if (u8_item_idx < (u8_num_of_items - 1)) {
      while (*pac_str != ',') {
        // Need to check for end of string first
        if (*pac_str == 0) {
          // End of string array -> not expected
          e_err_code = USC_ERROR_INVALID_PARAMETER;
          break;
        } else {
          // Move to next character
          pac_str++;
        }
      }
      // Out if while -> found comma or null character (end of string). In
      // either way, pointer to string shall be increased. In case of failure,
      // function will quit anyways. In case that it was comma, program jump to
      // the begin and try to convert value after comma
      pac_str++;
    }

    if (e_err_code) {
      break;
    } else {
      // Increase pointer to output array, so new number will not overwrite
      // previous one
      pi32_buffer++;
    }
  }

  return e_err_code;
}

te_usc_error_code usu_str_dec_to_int_32(const char *pac_str,
                                        int32_t *pi32_result) {
  long l_result;

  te_usc_error_code e_err_code = _str_to_long(pac_str, &l_result, 10);
  if (!e_err_code) {
    // No error -> write result
    *pi32_result = (int32_t)l_result;
  }

  return e_err_code;
}

te_usc_error_code usu_str_dec_to_uint_32(const char *pac_str,
                                         uint32_t *pu32_result) {
  long l_result;

  te_usc_error_code e_err_code = _str_to_long(pac_str, &l_result, 10);
  if (!e_err_code) {
    // No error -> write result
    *pu32_result = (uint32_t)l_result;
  }

  return e_err_code;
}

te_usc_error_code usu_str_hex_to_uint_32(const char *pac_str,
                                         uint32_t *pu32_result) {
  long l_result;

  te_usc_error_code e_err_code = _str_to_long(pac_str, &l_result, 16);
  if (!e_err_code) {
    // No error -> write result
    *pu32_result = (uint32_t)l_result;
  }

  return e_err_code;
}

te_usc_error_code usu_str_hex_to_bytes(const char *pac_str,
                                       const uint16_t u16_buffer_size,
                                       uint8_t *pau8_bytes,
                                       uint16_t *pu16_num_of_bytes) {
  // Initial check
  if (!pac_str || !pau8_bytes) {
    // Empty pointer
    return USC_ERROR_INVALID_PARAMETER;
  }
  if (u16_buffer_size == 0) {
    // At least 1 Byte is required
    return USC_ERROR_NO_MEMORY;
  }

  // For decoding hexadecimal value we need to know it's length (need to know
  // number of digits). If length will be even, then we can start processing
  // without doubts. But in case of odd, assume that there was leading zero
  uint16_t u16_num_of_digits = usu_get_word_length(pac_str);

  if (u16_num_of_digits == 0) {
    // There are no data
    return USC_ERROR_NOT_FOUND;
  }

  // Basically index for characters from input string
  uint16_t u16_char_idx = 0;

  // Number of Bytes written into buffer. So far zero
  *pu16_num_of_bytes = 0;

  // Keep error code somewhere
  te_usc_error_code e_err_code = USC_ERROR;

  // When parsing HEX from string, need 2 parts. Low 4 bits and high 4 bits
  uint8_t u8_low, u8_high;

  // Even numbers have set bit at LSb
  if (u16_num_of_digits & 0x0001) {
    // Number is even -> process it as lower 4 bits
    e_err_code = usu_char_hex_to_int(pac_str[u16_char_idx++],
                                     &pau8_bytes[*pu16_num_of_bytes++]);
    if (e_err_code) {
      return e_err_code;
    }
  } else {
    // Number is odd
  }

  // Fill the "Bytes" buffer
  for (; u16_char_idx < u16_num_of_digits; u16_char_idx += 2) {
    if (*pu16_num_of_bytes >= u16_buffer_size) {
      // Can not write more data -> report error
      return USC_ERROR_NO_MEMORY;
    }

    // Process 2 characters (1 Byte)
    e_err_code = usu_char_hex_to_int(pac_str[u16_char_idx], &u8_high);
    e_err_code |= usu_char_hex_to_int(pac_str[u16_char_idx + 1], &u8_low);

    // If there is problem, report it
    if (e_err_code) {
      return e_err_code;
    }

    // Else keep processing
    pau8_bytes[*pu16_num_of_bytes] = (u8_high << 4) | (u8_low << 0);

    *pu16_num_of_bytes += 1;
  }

  // When processed, everything should be just fine
  return USC_OK;
}

te_usc_error_code usu_bytes_to_str_hex(const uint8_t *pau8_bytes,
                                       const uint16_t u16_num_of_bytes,
                                       const char c_separator, char *pac_str,
                                       const uint16_t u16_buffer_size) {
  // Check pointers
  if (!pau8_bytes || !pac_str) {
    return USC_ERROR_INVALID_PARAMETER;
  }

  // Initial check. For every Byte is required 2 characters + 1 NULL. If
  // separator is defined, then calculate with N-1 separator characters
  uint16_t u16_required_buffer_size = u16_num_of_bytes * 2;
  if (c_separator) {
    u16_required_buffer_size += u16_num_of_bytes - 1;
  }

  if (u16_buffer_size <= u16_required_buffer_size) {
    return USC_ERROR_NO_MEMORY;
  }

  // Use own pointer to access string
  char *pac_str_work = pac_str;

  te_usc_error_code e_err_code = USC_ERROR;

  for (uint16_t u16_processed_byte_idx = 0;
       u16_processed_byte_idx < u16_num_of_bytes; u16_processed_byte_idx++) {
    // This should not fail, but just in case
    e_err_code =
        usu_int_to_char_hex(pau8_bytes[u16_processed_byte_idx], pac_str_work);
    if (e_err_code) {
      return e_err_code;
    }

    // Move pointer in string by 2 characters
    pac_str_work += 2;

    // If there is separator defined and not in last iteration, add it
    if (c_separator) {
      if (u16_processed_byte_idx < (u16_num_of_bytes - 1)) {
        *pac_str_work = c_separator;
        pac_str_work++;
      }
    }
  }

  // Add terminating NULL
  *pac_str_work = 0;

  return USC_OK;
}
// ========================| Middle level functions |=========================
uint16_t usu_get_word_length(const char *pac_str) {
  uint16_t u16_word_length = 0;

  while (1) {
    // If space or NULL found, exit
    if ((pac_str[u16_word_length] == ' ') || (pac_str[u16_word_length] == 0)) {
      return u16_word_length;
    } else {
      // Else keep searching. Unless we reached maximum
      if (++u16_word_length == 0xFFFF) {
        // Reached maximum -> return maximum
        return u16_word_length;
      }
    }
  }  // "Infinite" while
}
// ==========================| Low level functions |==========================
te_usc_error_code usu_str_hex_2_byte_to_int(const char *pac_str,
                                            uint8_t *pu8_result) {
  // Take two characters (low and high 4 bits) and convert them
  te_usc_error_code e_err_code;
  uint8_t u8_high_4_bits, u8_low_4_bits;

  e_err_code = usu_char_hex_to_int(pac_str[0], &u8_high_4_bits);
  if (e_err_code) {
    return e_err_code;
  }

  e_err_code = usu_char_hex_to_int(pac_str[1], &u8_low_4_bits);
  if (e_err_code) {
    return e_err_code;
  }

  // If program get there, conversions were successful -> calculate result
  *pu8_result = (u8_high_4_bits << 4) + u8_low_4_bits;

  return USC_OK;
}

te_usc_error_code usu_char_hex_to_int(const char c_hex, uint8_t *pu8_result) {
  // 0~9
  if ((c_hex >= '0') && (c_hex <= '9')) {
    // Char is just integer -> decrease ACSII "0" and get integer result
    *pu8_result = c_hex - '0';
    return USC_OK;
  } else if ((c_hex >= 'A') && (c_hex <= 'F')) {
    // Same as before - "Minus A" is the key. Since A means 10 -> +10
    *pu8_result = c_hex - 'A' + 10;
    return USC_OK;
  } else if ((c_hex >= 'a') && (c_hex <= 'f')) {
    *pu8_result = c_hex - 'a' + 10;
    return USC_OK;
  }

  return USC_ERROR_INVALID_PARAMETER;
}

te_usc_error_code usu_int_to_char_hex(const uint8_t u8_byte, char *pc_hex) {
  // Need to process high and low 4 bits separately/ Start with MSb
  te_usc_error_code eErrCode = _4_bit_val_to_hex_char(u8_byte >> 4, pc_hex++);
  if (eErrCode) {
    return eErrCode;
  }

  // Lower 4 bits
  eErrCode = _4_bit_val_to_hex_char(u8_byte & 0x0F, pc_hex);

  return eErrCode;
}

// ==========================| Internal functions |===========================
static te_usc_error_code _str_to_long(const char *pac_str, long *pl_result,
                                      const int i_base) {
  char *p_dummy;

  long l_converted = strtol(pac_str, &p_dummy, i_base);

  if (errno == ERANGE) {
    // Value out of range
    return USC_ERROR_INVALID_PARAMETER;
  }

  // If no data were processed, then actually result will be zero and no error
  // is raised. The "dummy" pointer will match with input string then.
  if (p_dummy == pac_str) {
    // strtol() could not convert it, but it did not raised error
    return USC_ERROR_INVALID_PARAMETER;
  }

  // If does not fit into 32 bit value
  if (l_converted > 0xFFFFFFFF) {
    return USC_ERROR_INVALID_PARAMETER;
  }

  *pl_result = l_converted;
  return USC_OK;
}

static te_usc_error_code _4_bit_val_to_hex_char(const uint8_t u8_val_4_bit,
                                                char *pc_ascii_hex) {
  // If value is higher than 4 bit, return error code
  if (u8_val_4_bit & 0xF0) {
    return USC_ERROR_INVALID_PARAMETER;
  }

  // Other wise value is 4 bit long -> 0 ~ 15 (0 ~ F)

  if (u8_val_4_bit < 10) {
    // Cheap trick. Use "first" ASCII character and shift it
    *pc_ascii_hex = '0' + u8_val_4_bit;
  } else {
    // Need to get difference between 0x0A and current value, so offset trick
    // will work.
    *pc_ascii_hex = 'A' + (u8_val_4_bit - 0x0A);
  }

  return USC_OK;
}
